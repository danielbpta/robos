# -*- coding: utf-8 -*-
# Autor : Daniel Baptista
# Data: 19/07/2020
import scrapy
import re
import json


class RgrSpider(scrapy.Spider):
    name = 'RGR'
    
    
    def __init__(self, filename=None):
        if filename:
            with open(filename, 'r') as f:
                self.start_urls = f.readlines()
   
     
    def parse(self, response):
        lista = []
        itens = response.css('div')
        if len(itens) > 0:
            logo = response.css('a > img::attr(src)').extract_first()
            telefones = response.css('a').re(r'>([0-9\(\)+ -]+)<')
            if telefones:
                for telefone in telefones:
                    tel = telefone.replace('-',' ').strip()
                    lista.append(tel)
            else:
                telefones = response.css('p').re(r'>\r\n([0-9\(\)+ -]{10,20})[\r\n|]*<')
                for telefone in telefones:
                    tel = telefone.replace('-',' ').strip()
                    lista.append(tel)
            web = response.url
            url = re.findall(r'(https://[a-z\.]*)/', web)

            if (logo.startswith('//')): 
                logotipo = re.findall(r'(/[A-z0-9\.\/-]*)', logo)
                json = {'logo': 'https:' + logotipo[0], 'telefones': lista, 'website': url[0],}
                print(json)
            if not (logo.startswith('http://') or logo.startswith('https://')): 
                logotipo = re.findall(r'(/[A-z0-9\.\/-]*)', logo)
                json = {'logo': url[0] + logotipo[0], 'telefones': lista, 'website': url[0],}
                print(json)
            else:
                logotipo = re.findall(r'(https://[A-z0-9\.\/-]*)', logo)
                json = {'logo': logotipo[0], 'telefones': lista, 'website': url[0],}
                print(json)
        else:
            print(f'Dados não encontrado na url {response.url}')
    
    
   


    
